" Plugins directory
call plug#begin(stdpath('data') . '/plugged')

" Vim Utilities
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'

" Setup
Plug 'kyazdani42/nvim-tree.lua'

" Visuals
Plug 'tomasiser/vim-code-dark'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'airblade/vim-gitgutter'

" Information
Plug 'itchyny/lightline.vim'

" Search
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" Completion
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-nvim-lua'
Plug 'hrsh7th/vim-vsnip'
Plug 'lukas-reineke/format.nvim'

" LSP
"Plug 'neovim/nvim-lspconfig'
"Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}


" Initialize plugin system
call plug#end()

" ------------- General Settings -------------

set encoding=UTF-8
set fileencoding=UTF-8
scriptencoding utf-8

let mapleader = ','

set smarttab
set cindent
set backspace=2                  " Make backspace work like most other applications
set expandtab                    " Expand tabs into spaces
set nowrap                       " Do not wrap lines
set relativenumber               " Turn on relative numbers
set nu
set shiftwidth=4                 " Shift by 4 spaces
set tabstop=4                    " Tabs are 4 spaces
" if hidden is not set, TextEdit might fail.
set hidden " Some servers have issues with backup files, see #649 set nobackup set nowritebackup " Better display for messages set cmdheight=2 " You will have bad experience for diagnostic messages when it's default 4000.
set noshowmode
set colorcolumn=80
set scrolloff=13
set updatetime=500

" always show signcolumns
set signcolumn=yes

" Better compatibility with file watchers
set nobackup
set nowritebackup

colorscheme codedark

" Search Settings
set nohlsearch
set incsearch
set ignorecase
set smartcase
noh

" ---------- Commands ----------

" Aliases

" Show trailing whitespace
:hi ExtraWhitespace ctermbg=red guibg=red
:match ExtraWhitespace /\s\+$/

" Remaps

" --> Normal Mode

" vv to generate new vertical split
nnoremap <silent> vv <C-w>v

" j/k will move virtual lines (lines that wrap)
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

" Telescope keys
" nnoremap <C-p> <cmd>Telescope find_files<cr>
" nnoremap <C-f> <cmd>Telescope live_grep<cr>
" nnoremap <C-b> <cmd>Telescope buffers<cr>
" nnoremap <leader>cs <cmd>Telescope colorscheme<cr>

" Shortcuts for system clipboard
nnoremap <leader>p "+p
vnoremap <leader>y "+y

" Navigate between panes
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Scroll buffer faster
nnoremap <C-e> 3<C-e>
nnoremap <C-y> 3<C-y>

" Save with \s and quit with \q
nnoremap <leader>q :q<cr>
nnoremap <leader>s :w<cr>

" Buffer management
nnoremap <leader><leader> :b#<cr>
nnoremap <leader>d :b#<cr> :bd#<cr>

" File Explorer
" nnoremap <silent> <leader>ee :NvimTreeToggle<cr>
" nnoremap <silent> <leader>ef :NvimTreeFindFile<cr>

" Reload vimrc with \l
nnoremap <leader>l :source ~/.config/nvim/init.vim<cr>

" Toggle paste
nnoremap <leader>z :set paste!<cr>

" Change tabs with Shift + h or l
nnoremap <S-l> gt
nnoremap <S-h> gT

" IDE/LSP things
"nnoremap <leader>f <cmd>lua vim.lsp.buf.formatting()<CR>
"nnoremap <leader>rn <cmd>lua vim.lsp.buf.rename()<CR>
"nnoremap <space>e <cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>
"nnoremap gd <cmd>Telescope lsp_definitions<CR>
"nnoremap gr <cmd>Telescope lsp_references<CR>
"nnoremap <leader>b <cmd>Telescope buffers<CR>
"
"lua << EOF
"local nvim_lsp = require('lspconfig')
"
"nvim_lsp.gopls.setup {}
"nvim_lsp.jsonls.setup {
"  commands = {
"    Format = {
"      function()
"        vim.lsp.buf.range_formatting({},{0,0},{vim.fn.line("$"),0})
"      end
"    }
"  }
"}
"nvim_lsp.tsserver.setup {}
"EOF
"
"lua << EOF
"local actions = require('telescope.actions')
"-- Global remapping
"------------------------------
"require('telescope').setup{
"  defaults = {
"    mappings = {
"      i = {
"        ["<C-j>"] = actions.move_selection_next,
"        ["<C-k>"] = actions.move_selection_previous,
"        ["<esc>"] = actions.close,
"      },
"      n = {
"        ["<esc>"] = actions.close,
"      },
"    },
"  }
"}
"EOF
"
"lua <<EOF
"  local cmp = require('cmp')
"  cmp.setup {
"    snippet = {
"      expand = function(args)
"        -- You must install `vim-vsnip` if you use the following as-is.
"        vim.fn['vsnip#anonymous'](args.body)
"      end
"    },
"
"    -- You must set mapping if you want.
"    mapping = {
"      ['<C-p>'] = cmp.mapping.select_prev_item(),
"      ['<C-n>'] = cmp.mapping.select_next_item(),
"      ['<C-d>'] = cmp.mapping.scroll_docs(-4),
"      ['<C-f>'] = cmp.mapping.scroll_docs(4),
"      ['<C-Space>'] = cmp.mapping.complete(),
"      ['<C-e>'] = cmp.mapping.close(),
"      ['<CR>'] = cmp.mapping.confirm({
"        behavior = cmp.ConfirmBehavior.Insert,
"        select = true,
"      })
"    },
"
"    -- You should specify your *installed* sources.
"    sources = {
"      { name = 'buffer' },
"    },
"  }
"EOF
"
"" Setup buffer configuration (nvim-lua source only enables in Lua filetype).
"autocmd FileType lua lua require'cmp'.setup.buffer {
"\   sources = {
"\     { name = 'buffer' },
"\     { name = 'nvim_lua' },
"\   },
"\ }
"
"" Treesitter
"lua <<EOF
"require'nvim-treesitter.configs'.setup {
"    ensure_installed = {
"        "go",
"        "elm",
"        "javascript",
"        "typescript",
"        "tsx",
"        "yaml",
"        "json",
"        "html",
"        "css",
"        "bash",
"        "lua",
"        "php",
"        "dockerfile"
"    },
"    highlight = {
"        enable = true, -- false will disable the whole extension
"    },
"    indent = {
"      enable = true,
"    }
"}
"EOF
"
"" Folding
"set foldmethod=expr
"set foldexpr=nvim_treesitter#foldexpr()
"set foldlevelstart=99
"
"" Eslint formatting and general whitespace clearing
"lua <<EOF
"require'format'.setup {
"    ["*"] = {
"        {cmd = {"sed -i 's/[ \t]*$//'"}} -- remove trailing whitespace
"    },
"    javascript = {
"        {cmd = {"./node_modules/.bin/eslint --fix"}} -- use eslint for formatting in react
"    },
"}
"EOF
"
"
"" format on save with 'format' package
"augroup Format
"    autocmd!
"    autocmd BufWritePost * FormatWrite
"augroup END

