# Configurator

This is how I manage my dotfiles on nix(ish) systems using the ideas outlined in [this Atlassian blog post](https://www.atlassian.com/git/tutorials/dotfiles).

## Setup

Run the following command to configure a new system to use this dotfiles repository:

```bash
curl -Lks https://gitlab.com/ColtonHurt/configurator/-/snippets/2535583/raw/main/cfg.sh > cfg-setup.sh | /bin/bash cfg-setup.sh
```

## Usage

```bash
cfg <git commands>
```

## Dependencies

- zsh
- astronvim
- fzf
- FontAwesome (font)
- Source Code Pro (font)
- alacritty (terminal)
- gopls
- jsonls
- tsserver

## Compiled list of tools to setup when moving to another Unix system

- git
- caffeine
- node
- yarn/npm
- bun
- go
- kitty
- starship
- neovim
- ripgrep
- tmux
- coc
- python3
- bat
- lazydocker
- lazygit

## Pacman Install 1 liner

```bash
sudo pacman -Syu && sudo pacman -S zsh kitty starship vim neovim nodejs npm yarn python3 ripgrep neovim-plug python-pynvim bat lazydocker lazygit bottom gdu
```

### Optional

- Arduino
- Discord
- go Diskusage() - see astronvim docs
- bottom - see astronvim docs

## Setup Steps
1. Update to latest system
2. Download Chrome
3. Create ssh key for gitlab (https://docs.gitlab.com/ee/user/ssh.html)
4. Install dependencies (see pacman one liner)
5. Install oh-my-zsh (https://ohmyz.sh/#install)
6. Use configurator
7. Setup astronvim
8. Install and setup go env (https://go.dev/doc/install)
8. Install bun and optional deps