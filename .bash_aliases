#!/bin/sh

alias v='nvim'
alias vconfig="nvim ~/.config/nvim/init.vim"
alias la='ls -a'
alias ll='ls -alF'
alias config='v ~/.bash_aliases && source ~/.bashrc'

alias adb_restart='./Android/Sdk/platform-tools/adb kill-server && sudo ./Android/Sdk/platform-tools/adb start-server'
alias gf='git fetch'
alias gs='git status'
alias gall='git add -A'
alias gcm="git commit -m $argv"
alias gp='git pull'
alias gps='git push'
alias gd='git diff'
alias grs='git reset --hard HEAD'

alias ls='exa --long --icons --no-permissions --no-user --git --time-style long-iso --time=modified --group-directories-first -a'
alias tree='exa --tree'

alias ssh_ctrain_personal_server='ssh ctrain@167.71.105.216'

alias cfg="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"
alias cmeet='cat ~/meetings.txt'

alias discord_up='sudo pacman -R discord && sudo pacman -S discord'

alias ahint='bat ~/.bash_aliases'
